import { Component, OnInit } from '@angular/core';
import {TicketdataService} from '../../services';
import {SEVERITYLOW, SEVERITYHIGH, TICKET_TYPE_TTT, TICKET_TYPE_BATL} from '../../../../shared-module/constants';
import {LoginDetail} from '../../model';

@Component({
  selector: 'ea-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.css']
})
export class TicketDetailComponent implements OnInit {

  // other Variable
  ticketId: string;
  title: string;
  ticketType: any;
  severity: any;
  status: string;
  remarks: string;

  ticketallDetail: LoginDetail[];
  constructor(private http: TicketdataService) { }

  // initialization method
  ngOnInit(): void {
    this.ticketDetail();
  }

  // get data from API method
  ticketDetail = () => {
    const dataId = localStorage.getItem('DataId');
    this.http.getLoginDetail(dataId).subscribe((result) => {
      this.ticketallDetail = result;
      this.ticketId = result.payload.data[`ticket_id`];
      this.title = result.payload.data[`title`];
      this.ticketType = result.payload.data[`ticket_type`];
      this.severity = result.payload.data[`severity`];
      this.status = result.payload.data[`status`] ? 'this.status' : '-';
      this.remarks = result.payload.data[`remarks`];

      if (this.severity === 1) {
        this.severity = SEVERITYLOW;
      } else {
        this.severity = SEVERITYHIGH;
      }

      if (this.ticketType === 1) {
        this.ticketType = TICKET_TYPE_TTT;
      } else {
        this.ticketType = TICKET_TYPE_BATL;
      }
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import {TicketdataService} from '../../services';
import {ALLCONSTANT} from '../../../../shared-module/constants';



@Component({
  selector: 'ea-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.css']
})
export class CreateTicketComponent implements OnInit {


  // other variable
  selectValueDropdown: string;
  selectAssets: string;
  selectSeverity: string;


  // Constant variable
  dataCategory = ALLCONSTANT.DATADROPDOWN;
  severity = ALLCONSTANT.SEVIRITYDATA;
  assetStrading = ALLCONSTANT.ASSETSTRADING;


  // formgroup
  addTicket = new FormGroup({
    title: new FormControl('', Validators.required),
    area: new FormControl('', Validators.required),
    sto: new FormControl('', Validators.required),
    trading: new FormControl('', Validators.required),
    sevirity: new FormControl('', Validators.required),
  });

  constructor(private ticketService: TicketdataService) { }

  // initialization method
  ngOnInit(): void {
    this.changeVisibility();
  }

  changeVisibility = () => {
    this.addTicket.get('trading').disable();
  }

  // select dropdown method
  selectChange = (event) => {
    this.selectValueDropdown = event.target.value;
    if (this.selectValueDropdown !== '0') {
      this.addTicket.get('trading').enable();
    }
  }

  // select severity method
  selectChangeSeverity = (event) => {
    this.selectSeverity = event.target.value;
  }

  // select trading method
  selectChangeAssetsTrading = (event) => {
    this.selectAssets = event.target.value;
  }

  // data submit
  dataSubmit = () => {
    this.ticketService.createTicket(this.addTicket.value.title,
                                    this.selectValueDropdown,
                                    this.selectAssets,
                                    this.selectSeverity,
                                    this.addTicket.value.area).subscribe((result) => {
      console.log(result);
    });

  }
}



















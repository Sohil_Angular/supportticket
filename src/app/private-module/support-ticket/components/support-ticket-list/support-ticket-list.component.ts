import { Component, AfterViewInit, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {TicketData} from '../../model';
import { ALLCONSTANT, DEMOCOUNTER, PAGESIZE, TOTALPAGECOUNTER, SORT_ASC, SORT_DESC, LOADING_TRUE, LOADING_FALSE} from '../../../../shared-module/constants';
import { fromEvent, merge, throwError } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { tap } from 'rxjs/operators';
import { TicketdataService } from '../../services';
import { CreateTicketComponent } from '../create-ticket/create-ticket.component';
import {TicketDetailComponent} from '../ticket-detail/ticket-detail.component';

@Component({
  selector: 'ea-support-ticket-list',
  templateUrl: './support-ticket-list.component.html',
  styleUrls: ['./support-ticket-list.component.css']
})
export class SupportTicketListComponent implements AfterViewInit, OnInit {
// other variable
ticket: TicketData[];
totalRecords;
demoCounter: number = DEMOCOUNTER;
searchValueInput;
timer;

// Page Event Variable
pageEvent: PageEvent;


// pagination variable
postPerPage: number;
pageSize: number = PAGESIZE;
pageOption = ALLCONSTANT.PAGEOPTION;
totalPageCounter: number = TOTALPAGECOUNTER;
pagineChangeEvent;
dataSource: MatTableDataSource<TicketData>;

// Angular variable
@ViewChild(MatSort) sort: MatSort;
@ViewChild(MatPaginator) paginator: MatPaginator;
@ViewChild('input') input: ElementRef;

// sorting variable
storeOrder: string;
displayedColumns = ALLCONSTANT.DISPLAYCOLUMN;
order = true;
sortChangeEvent;

// loading bool
isLoadingResults;

constructor(private http: TicketdataService,
            private dialog: MatDialog,
            private route: ActivatedRoute,
            private router: Router) {}

ngAfterViewInit(): void{
  this.eventMerge();
}

// event merge method
eventMerge = () => {
  fromEvent(this.input.nativeElement, 'keyup')
    .pipe(
      tap(() => {
        this.apicallMethod();
      })
    )
    .subscribe();
  merge(this.sort.sortChange, this.paginator.page)
    .pipe(
      tap(() => this.apicallMethod())
    )
    .subscribe();
}

onPaginate = (pageEvent) => {
  this.pagineChangeEvent = pageEvent;
  this.postPerPage = +pageEvent.pageSize;
  this.totalPageCounter = this.postPerPage;

  if (pageEvent.pageIndex !== 0) {
    if (pageEvent.pageIndex > pageEvent.previousPageIndex) {
      this.demoCounter += DEMOCOUNTER;
    } else {
      this.demoCounter -= DEMOCOUNTER;
    }
  } else {
    this.demoCounter = DEMOCOUNTER;
  }
}


// initialization method
ngOnInit(): void {
  this.initialSortingOrder();
  this.initializeMethod();
}

initialSortingOrder = () => {
  this.storeOrder = SORT_ASC;
}

// ---- for search----
onSearchChange(searchValue): void {
  this.searchValueInput = searchValue.target.value;
  if (this.searchValueInput === '') {
    this.demoCounter = DEMOCOUNTER;
  }
}


apicallMethod = () => {
  // api call
  this.isLoadingResults = LOADING_TRUE;
  this.timer = setTimeout(() => {
    this.isLoadingResults = LOADING_FALSE;
    this.initializeMethod();
  }, 500);
}

// API parameters
getParameters = () => {
  const params = {};
  params[`recordsPerPage`] = this.totalPageCounter;
  params[`pageNumber`] = this.demoCounter;
  params[`sortOrder`] = this.storeOrder;
  params[`sortBy`] = 'created_at';
  params[`showAll`] = false;
  params[`search`] = { title: this.searchValueInput };

  return params;
}


// API CALL & GET DATA FROM API
public initializeMethod = () => {
  const param = this.getParameters();

  this.http.getTicketList(param).subscribe((result) => {
    this.dataSource = new MatTableDataSource(result.payload.data);
    this.totalRecords = result.pager.totalRecords;
    this.ticket = result.payload.data;
  });
}


// sorting
sortList = (sortChange) => {
  if (this.order) {
    this.storeOrder = SORT_DESC;
  } else {
    this.storeOrder = SORT_ASC;
  }
  this.demoCounter = DEMOCOUNTER;
  this.sortChangeEvent = sortChange;
  this.order = !this.order;
  this.paginator.firstPage();
}

// CreateTicket open Dialog
openDialog = () => {
  const dialogConfig = new MatDialogConfig();
  dialogConfig.width = '60%';
  const dialogCons = this.dialog.open(CreateTicketComponent, dialogConfig);
  dialogCons.afterClosed().subscribe(result => {
    this.apicallMethod();
  });
}

// Ticket detail open Dialog method
openDialogNotLogin = (id) => {
  localStorage.setItem('DataId', id);
  this.dialog.open(TicketDetailComponent);
}
}

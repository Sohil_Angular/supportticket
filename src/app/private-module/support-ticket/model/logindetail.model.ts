export interface LoginDetail{
    ticket_id: number;
    title: string;
    ticket_type: number;
    severity: number;
    status: string;
    created_at: string;
    remarks: string;
}

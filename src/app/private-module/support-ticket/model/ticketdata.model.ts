export interface TicketData {
        ticket_id: number;
        title: string;
        last_updated_at: Date;
        created_at: Date;
        status: string;
}

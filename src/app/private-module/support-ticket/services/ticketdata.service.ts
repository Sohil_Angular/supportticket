import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_URL } from '../../../shared-module/constants/ea-api.constants';

@Injectable({
  providedIn: 'root'
})
export class TicketdataService {

  constructor(private http: HttpClient) { }

  // getticket list APi
  getTicketList(params): Observable<any> {
    const httpheaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    // debugger
    return this.http.post<any>(API_URL.LIST, params, { headers: httpheaders });
  }


  getLoginDetail(id: string): Observable<any> {
    return this.http.get<any>(API_URL.NOT_ABLE_TO_LOGIN_AND_CREATE_TICKET + id);
  }

  // create ticket
  createTicket(recTicket, recDropdown, recAssets, recSeverity, recArea): Observable<any> {
    const formData = new FormData();
    formData.append('Content-Type', 'application/json');
    formData.append('title', recTicket);
    formData.append('description', recArea);
    formData.append('severity', recSeverity);
    formData.append('ticket_type', recDropdown);
    formData.append('digital_asset', recAssets);
    return this.http.post<any>(API_URL.NOT_ABLE_TO_LOGIN_AND_CREATE_TICKET, formData);
  }
}

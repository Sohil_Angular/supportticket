import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ROUTEPATHCONSTANTS } from 'src/app/shared-module/constants';
import { RouterModule, Routes } from '@angular/router';
import { SupportTicketListComponent } from '../../components';

const routes: Routes = [
  {
    path: ROUTEPATHCONSTANTS.REDIRECT_PATH,
    component: SupportTicketListComponent
  },
  ];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class SupportRoutingModule { }

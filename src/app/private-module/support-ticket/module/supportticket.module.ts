import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {ROUTEPATHCONSTANTS} from '../../../shared-module/constants';
import {CreateTicketComponent, SupportTicketListComponent, TicketDetailComponent} from '../components';
import {TicketdataService} from '../services';
import {SupportRoutingModule} from '../module/support-routing/support-routing.module';

@NgModule({
  declarations: [
    CreateTicketComponent,
    SupportTicketListComponent,
    TicketDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatProgressSpinnerModule,
    SupportRoutingModule
  ], exports: [MatDialogModule, MatGridListModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatButtonModule,
    MatPaginatorModule, MatSortModule, MatTableModule],
    providers: [TicketdataService],

})
export class SupportticketModuleModule {
  constructor(){
    console.log('SupportTicketModule');
  }
}

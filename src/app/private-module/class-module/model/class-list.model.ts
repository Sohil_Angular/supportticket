export interface ClassList{
    class_name: string;
    grade_id: number;
    parent_class_id: number;
    grade_master: ClasGradeMaster;
}
export interface ClasGradeMaster{
    alias: string;
    description: string;
    grade_id: number;
    name: string;
    is_active: number;
}

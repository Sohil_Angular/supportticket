export interface ClassDetailView{
    parent_class_id: number;
    provider_id: number;
    class_name: string;
    is_active: number;
    grade_id: number;
    grade_master: ClassDetailViewMAster;
}
export interface ClassDetailViewMAster{
    grade_id: number;
    name: string;
    alias: string;
    description: string;
}

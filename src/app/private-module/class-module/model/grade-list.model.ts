export interface GradeList {
    grade: Grade;
    grade_id: number;
    name: string;
}

interface Grade {
    alias: string;
    description: string;
    grade_id: number;
    is_active: number;
    name: string;
}

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ClassDataService } from '../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBaseComponent } from 'src/app/shared-module/components/form-base.component';
import { forkJoin, of } from 'rxjs';
import {GradeList} from '../../model'

@Component({
  selector: 'ea-create-class',
  templateUrl: './create-class.component.html',
  styleUrls: ['./create-class.component.css']
})
export class CreateClassComponent extends FormBaseComponent implements OnInit {

  // other variable
  selectValueDropdown: string;

  isEditMode = false;
  classId: number;
  createClassUpdateClassForm: FormGroup;

  classFormDetail;
  selectedGrade;

  gradeList: GradeList[];


  constructor(private classDetaService: ClassDataService,
              private router: ActivatedRoute,
              private route: Router,
              public dialogRef: MatDialogRef<CreateClassComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              fb: FormBuilder
            ) { super(fb); }

  ngOnInit(): void {
   this.dataFromClassList();
  }

  dataFromClassList = () => {
    this.isEditMode = this.data[`edit`];
    this.classId = this.data[`classId`];
    this.createClassUpdateFormDetail();
    this.getData();
  }

  createClassUpdateFormDetail = () => {
    this.createClassUpdateClassForm = this.createForm({
      name: ['',Validators.required],
      grade: ['',Validators.required]
    });
  }

  onSubmitCreateClassUpdateClassForm = (form: FormGroup) => {
    console.log(this.onSubmit(form));
    console.log(form);
    if (this.onSubmit(form)) {
      form.value[`provider_id`] = 165;
      form.value[`class_name`] = form.value[`name`];
      // delete form.value['name'];
      // delete form.value['gradeFilter'];
      if (this.isEditMode) {
        this.editClass(form.value);
      } else {
        form.value[`grade_id`] = form.value[`grade`];
        // delete form.value['grade'];
        this.addClass(form.value);
      }
    }
  }

  onCloseDialog(flag: boolean): void {
    console.log(false);
    this.dialogRef.close(flag);
  }


  addClass = param => {
    console.log(param);
    this.classDetaService.createClass(param).subscribe(response => {
      console.log(response);
      this.onCloseDialog(true);
    });
  }

  editClass = param => {
    console.log(param);
    this.classDetaService.updateClassName(param, this.classId)
      .subscribe(response => {
        this.onCloseDialog(true);
      });
  }


  getData = () => {
    forkJoin([
      this.classDetaService.gradeList(),
      this.isEditMode ? this.classDetaService.showClassDetail(
        this.classId) : of([])
    ]).subscribe(allResponses => {
      this.handleGradeList(allResponses[0][`payload`]);
      if (this.isEditMode) {
        this.handelParentClassDetail(allResponses[1]);
      }
    });
  }

  handleGradeList = list => {
    this.gradeList = list;
    console.log(this.gradeList);
  }


  handelParentClassDetail = response => {
    this.classFormDetail = response[`payload`];
    this.createClassUpdateClassForm.patchValue({
      name: this.classFormDetail.class_name,
    });

    this.createClassUpdateClassForm.controls[`grade`].disable();
  }
}

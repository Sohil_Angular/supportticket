import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ClassDataService } from '../../services';
import { ROUTEPATHCONSTANTS, ALLCONSTANT, DEMOCOUNTER, LOADING_FALSE, LOADING_TRUE, PAGESIZE, SORT_ASC, SORT_DESC } from '../../../../shared-module/constants';
import { ClassList } from '../../model';
import { MatSort } from '@angular/material/sort';
import { from, fromEvent, merge } from 'rxjs';
import { catchError, debounceTime, startWith, switchMap, tap } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { throwError } from 'rxjs';
import { CreateClassComponent } from '../create-class/create-class.component';
import { getQueryParams } from 'src/app/shared-module/functions/common-functions';

@Component({
  selector: 'ea-class-list',
  templateUrl: './class-list.component.html',
  styleUrls: ['./class-list.component.css']
})
export class ClassListComponent implements AfterViewInit, OnInit {

  // other variable
  classData: ClassList[];
  totalRecords;
  resetDataCounter: number = DEMOCOUNTER;
  searchValueInput;
  timer;

  // Page Event Variable
  pageEvent: PageEvent;


  // pagination variable
  postPerPage: number;
  pageSize: number = PAGESIZE;
  totalPageCounter: number;
  pageOption = ALLCONSTANT.PAGEOPTION[1];
  recordsPerPage = ALLCONSTANT.PAGEOPTION[0];
  totalElements = 0;
  // pagineChangeEvent;
  dataSource: MatTableDataSource<any>;

  // sorting variable
  storeOrder: string;
  displayedColumns: string[] = ALLCONSTANT.CLASS_DISPLAYCOLUMN;
  orderSorting = true;
  sortChangeEvent;

  // search variable
  searchbool: boolean;

  // Angular variable
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('input') input: ElementRef;

  // loading bool
  isLoadingResults;
  rowNumber = 1;
  nameFilterField: FormControl = new FormControl('');

  constructor(private dialog: MatDialog,
              private classDetaService: ClassDataService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngAfterViewInit(): void {}

  ngOnInit(): void {

    this.initializeMethod();
  }


  // Page Events
  onPaginate = params => {
    console.log(params);

    this.recordsPerPage = params[`pageSize`];
    if (params[`pageIndex`] !== 0) {

      if (params[`pageIndex`] > params[`previousPageIndex`]) {
        this.rowNumber = this.rowNumber + this.recordsPerPage;
      } else {
        this.rowNumber = this.rowNumber - this.recordsPerPage;
      }
    }else{
      this.rowNumber = 1;
    }

    this.getParentClass();
  }


  getSearchParams = () => {
    const params = {};
    if (this.nameFilterField.value) {
      params[`class_name`] = this.nameFilterField.value;
    }
    return params;
  }

  openDialog(classId?): void {
    const dialogRef = this.dialog.open(CreateClassComponent,
      {
        data: { edit: !!classId, classId: (classId) }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.initializeMethod();
      }
    });
  }

  // view class detail
  openClassDetail = (classId) => {
    this.router.navigate(['/' + ROUTEPATHCONSTANTS.CLASS_DETAIL_PATH + classId]);
  }


  initializeMethod = () => {
    this.isLoadingResults = true;
    merge(
      this.sort.sortChange,
      this.nameFilterField.valueChanges,
    )
      .pipe(
        startWith(''),
        debounceTime(300),
        switchMap(() => {
          this.paginator.firstPage();
          return this.getClassList();
        }),
        catchError(error => {
          this.isLoadingResults = false;
          return throwError(error);
        })
      )
      .subscribe(response => {
        this.handleClassList(response[`payload`], response[`pager`]);
        this.isLoadingResults = false;
      });
  }

  handleClassList = (parentClassList, pager) => {
    this.classData = parentClassList;
    this.totalElements = pager[`totalRecords`];
  }

  getClassList = () => {
    return this.classDetaService.getClass({
      provider_id: 165,
      ...getQueryParams(
        this.getSearchParams(),
        this.sort,
        this.rowNumber,
        this.recordsPerPage
      )
    });
  }

  getParentClass = () => {
    this.isLoadingResults = true;
    this.getClassList().subscribe(
      response => {
        this.handleClassList(response[`payload`], response[`pager`]);
        this.isLoadingResults = false;
      },
      err => {
        this.isLoadingResults = false;
      }
    );
  }
}

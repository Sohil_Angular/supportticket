import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {ROUTEPATHCONSTANTS} from '../../../../shared-module/constants';
import {ClassDataService} from '../../services';
import {ClassDetailView} from '../../model';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'ea-class-detail',
  templateUrl: './class-detail.component.html',
  styleUrls: ['./class-detail.component.css']
})
export class ClassDetailComponent implements OnInit {

  // store class name & grade from api
  classNameStore: string;
  gradeStore;
  classId: number;
  isEditMode = false;
  currentParentClassId: number;
  classParentDetail: ClassDetailView;
  parentClassDetailsForm: FormGroup;

  constructor(private router: Router ,
              private classDataService: ClassDataService ,
              private rout: ActivatedRoute) { }

  ngOnInit(): void {
    this.initializationClassDetail();
  }



  // initializationClassDetail
  initializationClassDetail = () => {
    this.rout.paramMap.subscribe((result) => {
      this.classId = result[`params`][`id`];
    });
    this.classDataService.showClassDetail(this.classId).subscribe((result) => {
      this.handelClassDetail(result);
    });
  }

  handelClassDetail = response => {
    this.classParentDetail = response[`payload`];
  }
  // Back from Class view
  backToClassList(): void {
    this.router.navigate(['/' + ROUTEPATHCONSTANTS.CLASS_PATH ]);
  }
}

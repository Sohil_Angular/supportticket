import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatGridListModule } from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { RouterModule, Routes } from '@angular/router';
import {ROUTEPATHCONSTANTS} from '../../../shared-module/constants';
import {ClassDetailComponent, ClassListComponent, CreateClassComponent } from '../components';


const routes: Routes = [
  {
   path: ROUTEPATHCONSTANTS.REDIRECT_PATH,
   component: ClassListComponent
  },
  ];

@NgModule({
  declarations: [
    ClassListComponent,
    CreateClassComponent,
    ClassDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatTableModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    MatGridListModule,
    MatIconModule,
    MatMenuModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    RouterModule.forChild(routes)
  ],
  exports: [MatPaginatorModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class ClassModule {
  constructor(){
    console.log('ClassModule');
  }
 }

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import {API_URL} from '../../../shared-module/constants';

@Injectable({
  providedIn: 'root'
})
export class ClassDataService {

  // other variable
  accessTokenHeader = localStorage.getItem('accesstoken');

  constructor(private http: HttpClient) { }

  // get class
  getClass(params): Observable<any> {
    const httpheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: `Bearer ${this.accessTokenHeader}`,
    });

    return this.http.post<any>(API_URL.CLASS_LIST_API, params, { headers: httpheaders });
  }

  // create class
  createClass(params): Observable<any> {
    const httpheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: `Bearer ${this.accessTokenHeader}`,
    });

    return this.http.post<any>(API_URL.CREATE_CLASS_API, params, { headers: httpheaders });
  }

  // grade list
  gradeList(): Observable<any> {
    const httpheaders = new HttpHeaders({
      authorization: `Bearer ${this.accessTokenHeader}`,
    });
    return this.http.get<any>(API_URL.GRADE_LIST_API, { headers: httpheaders });
  }

  // update class name
  updateClassName(params, id): Observable<any> {
    const httpheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: `Bearer ${this.accessTokenHeader}`,
    });
    return this.http.put<any>(API_URL.UPDATE_API + id, params, { headers: httpheaders });
  }

  // show class detail
  showClassDetail(id): Observable<any> {

    const httpheaders = new HttpHeaders({
      authorization: `Bearer ${this.accessTokenHeader}`,
    });
    return this.http.get<any>(API_URL.CLASS_DETAIL_API + id, { headers: httpheaders });
  }



}

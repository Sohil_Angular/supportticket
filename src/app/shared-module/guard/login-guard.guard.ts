import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import {LoginService} from '../../public-module/login-module/service';
import {ROUTEPATHCONSTANTS} from '../constants';

@Injectable({
  providedIn: 'root'
})
export class LoginGuardGuard implements CanActivate {
  constructor(private router: Router, private loginService: LoginService) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean{
      if (!this.loginService.isAuthenticed()) {
        this.router.navigate(['/' + `${ROUTEPATHCONSTANTS.LOGIN_PATH}`]);
      }
      return true;
  }
}

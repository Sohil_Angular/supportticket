import {FormBuilder, FormGroup} from '@angular/forms';
import { Component,  OnChanges, OnInit } from '@angular/core';

@Component({
    selector: 'ea-form-base',
    template: ''
})
export class FormBaseComponent implements OnInit {

    constructor(protected fb: FormBuilder) {}

    ngOnInit(): void {}

    protected createForm(controlsConfig): FormGroup {
        const form = this.fb.group(controlsConfig);
        return form;
    }
     onSubmit = (form) => {
        if (form.invalid) {
            return false;
        }
        return true;
    }
}

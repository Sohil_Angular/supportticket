export const ALLCONSTANT = {

    // set the dopdown value
    DATADROPDOWN : [
    { id: 1, name: 'STO', },
    { id: 2, name: 'KYC', },
    { id: 3, name: 'Trade', },
    { id: 4, name: 'Tradeotc', },
    { id: 5, name: 'UserManagement', },
   ],

   // set sevirity in dropdown
  SEVIRITYDATA : [
  { id: 1, name: 'Low' },
  { id: 2, name: 'High' }
  ],

  // set trading value in dropdown
  ASSETSTRADING : [
    { id: 1, name: 'TTT' },
    { id: 2, name: 'BATL' }
    ],

    // set table column name
    DISPLAYCOLUMN : ['ticket_id', 'created_at', 'title', 'status'],

    CLASS_DISPLAYCOLUMN :  ['name', 'grade', '...'],

    // pagination itemperpage
    PAGEOPTION : [5, 10],


  };

// counter
export const DEMOCOUNTER = 1;

// pagination constant
export const PAGESIZE = 10;
export const TOTALPAGECOUNTER = 0;


// Create ticket severity
export const SEVERITYLOW = 'Low';
export const SEVERITYHIGH = 'High';

// Ticket Type
export const TICKET_TYPE_TTT = 'TTT';
export const TICKET_TYPE_BATL = 'BATL';

// Sort
export const SORT_DESC = 'desc';
export const SORT_ASC = 'asc';

// loading
export const LOADING_TRUE = true;
export const LOADING_FALSE = false;

// login flag
export const FLAG_CONST = 1;

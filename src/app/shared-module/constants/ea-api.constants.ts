import { environment } from 'src/environments/environment';

// Base URL get from environment
const API_BASE_URL = environment.apiBaseUrl;
const API_BASE_LOGIN_URL = environment.apiBaseloginUrl;

export const API_URL = {
    LIST : API_BASE_URL + '/list', // get Ticket List URL
    NOT_ABLE_TO_LOGIN_AND_CREATE_TICKET : API_BASE_URL + '/',  // create ticket and not able to login URL

    // Login URL
    LOGIN_API : API_BASE_LOGIN_URL + '/public/api/v1/login',
    // config URL
    CONFIG_API : API_BASE_LOGIN_URL + '/api/v1/auth/config',
    // class list
    CLASS_LIST_API : API_BASE_LOGIN_URL + '/api/v1/parent/class/list',
    // create class
    CREATE_CLASS_API : API_BASE_LOGIN_URL + '/api/v1/parent/class/add',
    // grade list
    GRADE_LIST_API : API_BASE_LOGIN_URL + '/api/v1/provider/grade/list',
    // update class name
    UPDATE_API : API_BASE_LOGIN_URL + '/api/v1/parent/class/edit/',
    // class detail
    CLASS_DETAIL_API : API_BASE_LOGIN_URL + '/api/v1/parent/class/details/',
};

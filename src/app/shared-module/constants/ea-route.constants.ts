export const ROUTEPATHCONSTANTS = {

    LOGIN_PATH : 'login',
    SUPPORT_TICKET_PATH : 'supportticket',
    CLASS_PATH : 'class',
    REDIRECT_PATH : '',
    CLASS_DETAIL_PATH : 'classdetail/view/',
};

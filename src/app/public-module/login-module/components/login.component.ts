import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {ProfileConfig, ProvideList} from '../model';
import {LoginService} from '../service';
import { FLAG_CONST } from '../../../shared-module/constants/ea-base.constants';
import { ROUTEPATHCONSTANTS } from '../../../shared-module/constants/ea-route.constants';

@Component({
  selector: 'ea-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // formGroup variable
  userDetail: FormGroup;

  // other variable
  storeEmail: string;
  storePassword: string;

  // ErrorHandler
  showErrors: string;

  profile: ProfileConfig[];
  providerList: ProvideList[];

  constructor(private loginService: LoginService, private routerActive: Router) { }

  ngOnInit(): void {
    this.initializationFormGroup();
  }

  initializationFormGroup = () => {
    this.userDetail = new FormGroup({
      useremail: new FormControl('', [Validators.required,
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      Validators.email]),
      userpassword: new FormControl('', [Validators.required]),
    });
    this.loginCheck();
  }

  // pass the params
  getParameters = () => {
    const params = {};
    params[`email`] = this.storeEmail;
    params[`password`] = this.storePassword;
    return params;
  }

  // login api call
  initializationMethod = () => {
    const params = this.getParameters();
    this.loginService.loginProvider(params).subscribe((result) => {
      console.log(result);

      const accessToken = result[`payload`][`accessToken`];
      const loginflag = FLAG_CONST;

      localStorage.setItem('accesstoken', accessToken);
      localStorage.setItem('userlogin', JSON.stringify(loginflag));

      this.profileConfigMethod();
      this.routerActive.navigate(['/' + `${ROUTEPATHCONSTANTS.SUPPORT_TICKET_PATH}`]);
    }, (error) => {
      this.showErrors = error.error.message;
      console.log(this.showErrors);
      throw error;
    });
  }

  // data submit
  onSubmit = (logindetail) => {
    this.storeEmail = logindetail.useremail;
    this.storePassword = logindetail.userpassword;
    this.initializationMethod();

  }

  // config api call
  profileConfigMethod = () => {
    this.loginService.profileConfig().subscribe((result) => {
      this.profile = result[`payload`];
      this.providerList = result[`payload`][`provider_list`];

      console.log(result);
      localStorage.setItem('app_id', this.profile[`app_id`]);
      localStorage.setItem('avatar', this.profile[`avatar`]);
      localStorage.setItem('contact_number', this.profile[`contact_number`]);
      localStorage.setItem('dial_code', this.profile[`dial_code`]);
      localStorage.setItem('dial_code', this.profile[`dial_code`]);
      localStorage.setItem('email', this.profile[`email`]);
      localStorage.setItem('first_name', this.profile[`first_name`]);
      localStorage.setItem('is_email_verified', this.profile[`is_email_verified`]);
      localStorage.setItem('is_mobile_verified', this.profile[`is_mobile_verified`]);
      localStorage.setItem('is_profile_completed', this.profile[`is_profile_completed`]);
      localStorage.setItem('last_name', this.profile[`last_name`]);
      localStorage.setItem('user_id', this.profile[`user_id`]);
      localStorage.setItem('user_role_link.is_active', this.profile[`user_role_link.is_active`]);
      localStorage.setItem('user_role_link.role_id', this.profile[`user_role_link.role_id`]);
      localStorage.setItem('user_uuid', this.profile[`user_uuid`]);

      console.log(this.providerList);
      for (const index in this.providerList) {
        if (index){

          const isActive = this.providerList[index][`is_active`];
          const name = this.providerList[index][`name`];
          const providerCode = this.providerList[index][`provider_code`];
          const providerId = this.providerList[index][`provider_id`];
          const providerUuid = this.providerList[index][`provider_uuid`];
          const status = this.providerList[index][`status`];
        }
      }
    });
  }

  loginCheck = () => {
    const getDatafromLocalStore = localStorage.getItem('userlogin');
    if (JSON.parse(getDatafromLocalStore) === 1) {
      this.routerActive.navigate(['/' + `${ROUTEPATHCONSTANTS.SUPPORT_TICKET_PATH}`]);
    } else {
      this.routerActive.navigate(['/' + `${ROUTEPATHCONSTANTS.LOGIN_PATH}`]);
    }
  }
}

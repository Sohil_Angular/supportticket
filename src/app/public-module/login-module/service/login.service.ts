import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {API_URL} from '../../../shared-module/constants';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  isAuthenticed(): boolean {
    const localstorevalue = localStorage.getItem('userlogin');
    if (JSON.parse(localstorevalue) === 1) {
      return true;
    } else {
      return false;
    }
  }

  // login provider api
  loginProvider(params): Observable<any> {
    return this.httpClient.post<any>(API_URL.LOGIN_API, params);
  }

  // profile config api
  profileConfig(): Observable<any> {
    const tokenHeader = localStorage.getItem('accesstoken');
    const header = new HttpHeaders({
      authorization : `Bearer ${tokenHeader}`,
    });
    return this.httpClient.get<any>(API_URL.CONFIG_API, { headers: header });
  }
}

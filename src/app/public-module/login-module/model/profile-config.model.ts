export interface ProfileConfig{
    app_id: number;
    avatar: string;
    contact_number: number;
    dial_code: number;
    dial_code_country_id: number;
    email: string;
    first_name: string;
    last_name: string;
    has_accepted_tnc: number;
    is_email_verified: number;
    is_mobile_verified: number;
    is_profile_completed: number;
    user_id: number;
    user_role_link_is_active: number;
    user_role_link_role_id: number;
    user_uuid: number;
}
export interface ProvideList{
    is_active: number;
    name: string;
    provider_code: string;
    provider_id: number;
    provider_uuid: string;
    status: number;
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ScrollDispatcher, ScrollingModule } from '@angular/cdk/scrolling';
import { LoginModule } from './public-module/login-module/module/login.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import {ResolverService} from '../app/private-module/class-module/services';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SupportRoutingModule } from './private-module/support-ticket/module/support-routing/support-routing.module';


@NgModule({
  declarations: [
    AppComponent
],
 imports : [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ScrollingModule,
    LoginModule,
    MatDialogModule,
    MatTableModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    // SupportRoutingModule
  ],
  providers: [ScrollDispatcher, ResolverService],
  bootstrap: [AppComponent],
    exports: [MatDialogModule, MatTableModule, MatButtonModule, MatSelectModule, MatInputModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {
  constructor() {
    console.log('App module');
  }
}





































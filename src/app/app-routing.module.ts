import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../app/public-module/login-module/components';
import { LoginGuardGuard } from '../app/shared-module/guard';
import { ROUTEPATHCONSTANTS } from '../app/shared-module/constants';
import { ClassDetailComponent } from '../app/private-module/class-module/components';
// import { ResolverService } from '../app/private-module/class-module/services';
import { ResolverService } from './private-module/class-module/services';
const routes: Routes = [
  {
    path: ROUTEPATHCONSTANTS.LOGIN_PATH,
    component: LoginComponent,
  },
  {
    path: ROUTEPATHCONSTANTS.SUPPORT_TICKET_PATH,
    loadChildren: () => import('./private-module/support-ticket/module/supportticket.module').then(m => m.SupportticketModuleModule),
    canActivate: [LoginGuardGuard],
  },
  {
    path: ROUTEPATHCONSTANTS.REDIRECT_PATH,
    component: LoginComponent,
    canActivate: [LoginGuardGuard],
  },
  {
    path: ROUTEPATHCONSTANTS.CLASS_PATH,
    loadChildren: () => import('./private-module/class-module/module/class.module').then(m => m.ClassModule),
    canActivate: [LoginGuardGuard],
    resolve: {
      result: ResolverService,
    }
  },
  {
    path: ROUTEPATHCONSTANTS.CLASS_DETAIL_PATH + ':id',
    component: ClassDetailComponent,
    canActivate: [LoginGuardGuard],
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
